var noteDict = [
  'E6',
  'D6',
  'C6',
  'B5',
  'A#5',
  'A5',
  'G#5',
  'G5',
  'F#5',
  'F5',
  'E5',
  'D#5',
  'D5',
  'C#5',
  'C5',
  'B4',
  'A#4',
  'A4',
  'G#4',
  'G4',
  'F#4',
  'F4',
  'E4',
  'D4',
  'C4',
  'B3',
  'A3',
  'G3',
  'D3',
  'C3'
];

const updateCanvas = () => {
  var canvas = document.getElementById('main');
  var ctx = canvas.getContext('2d');
  var xOffset = Number(document.getElementById('xOffset').value);
  var yOffset = Number(document.getElementById('yOffset').value);
  var cellWidth = Number(document.getElementById('cellWidth').value);
  var cellHeight = Number(document.getElementById('cellHeight').value);
  var quantizeMulti = Number(document.getElementById('quantizeMulti').value);
  var lineWidth = Number(document.getElementById('lineWidth').value);
  var highlightColor = document.getElementById('highlightColor').value;
  var highlightRadius = Number(document.getElementById('highlightRadius').value);

  if(midiData !== null) {
    canvas.width = xOffset + (cellWidth * midiData.length * quantizeMulti) + 50;
    canvas.height = yOffset + (cellHeight * 33) + 50;
    
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = '#000000';
    ctx.strokeStyle = '#000000';
    ctx.lineWidth = lineWidth;
    for(var x = 0;x < midiData.length * quantizeMulti;x++) {
      for(var y = 0;y < 29;y++) {
        ctx.strokeRect(
          xOffset + (cellWidth * x),
          yOffset + (cellHeight * y),
          cellWidth,
          cellHeight
        );
        if(y === 28 && x % 8 === 0) {
          ctx.beginPath();
          ctx.moveTo(
            xOffset + (cellWidth * x),
            yOffset + (cellHeight * y)
          );
          ctx.lineTo(
            xOffset + (cellWidth * x),
            yOffset + (cellHeight * y) + (cellHeight * 3)
          );
          ctx.stroke();
          ctx.font = `${cellHeight * 2}px Arial`;
          ctx.fillText(
            (x / 8) + 1,
            xOffset + (cellWidth * x),
            yOffset + (cellHeight * y) + (cellHeight * 5),
            cellWidth * 8,
            cellHeight * 2
          );
        }
      }
    }
    ctx.fillStyle = highlightColor;
    for(var n = 0;n < midiData.notes.length;n++) {
      var currNote = midiData.notes[n];
      var yIndex = 0;
      for(var d = 0;d < noteDict.length;d++) {
        if(currNote.name === noteDict[d]) {
          yIndex = d;
        }
      }
      ctx.beginPath();
      ctx.arc(
        xOffset + (cellWidth * currNote.start * quantizeMulti),
        yOffset + (cellHeight * yIndex),
        highlightRadius,
        0,
        2 * Math.PI
      );
      ctx.fill(); 
    }
  }
}

document.getElementById('updateCanvas').addEventListener('click', updateCanvas.bind(this));