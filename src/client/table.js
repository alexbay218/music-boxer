var inputData = {};
var outputData = {};

var midiDictionary = {
  'C6': 84,
  'B5': 83,
  'A#5': 82,
  'A5': 81,
  'G#5': 80,
  'G5': 79,
  'F#5': 78,
  'F5': 77,
  'E5': 76,
  'D#5': 75,
  'D5': 74,
  'C#5': 73,
  'C5': 72,
  'B4': 71,
  'A#4': 70,
  'A4': 69,
  'G#4': 68,
  'G4': 67,
  'F#4': 66,
  'F4': 65,
  'E4': 64,
  'D4': 62,
  'C4': 60,
  'B3': 59,
  'A3': 57,
  'G3': 55,
  'D3': 50,
  'C3': 48
};

const onOutputClick = (e) => {
  var idRegex = e.target.id.match(/o(\w#*\d)-(\d)/);
  var start = inputData.start + Number(idRegex[2]);
  var name = idRegex[1];
  var isActive = e.target.className === 'active';
  if(isActive) {
    if(outputData.notes){
      for(var i = 0;i < outputData.notes.length;i++) {
        if(outputData.notes[i].name === name && outputData.notes[i].start === start) {
          outputData.notes.splice(i, 1);
          i--;
        }
      }
    }
  }
  else {
    outputData.notes.push({
      name: name,
      midi: midiDictionary[name],
      start: start,
      duration: 1
    });
    playNote(name);
  }
  showData();
}

const table = () => {
  var table = document.getElementById('mainTable');
  table.innerHTML = '';
  var thead = table.createTHead();
  var row = thead.insertRow();
  var headers = [
    '<b>Input</b>',
    '<b>1</b>',
    '<b>2</b>',
    '<b>3</b>',
    '<b>4</b>',
    '<b>5</b>',
    '<b>6</b>',
    '<b>7</b>',
    '<b>8</b>',
    '<b>Output</b>',
    '<b>1</b>',
    '<b>2</b>',
    '<b>3</b>',
    '<b>4</b>',
    '<b>5</b>',
    '<b>6</b>',
    '<b>7</b>',
    '<b>8</b>'
  ];
  for(var i = 0;i < headers.length;i++) {
    row.insertCell().innerHTML = headers[i];
  }
  var inputRows = [
    'C8',

    'B7',
    'A#7',
    'A7',
    'G#7',
    'G7',
    'F#7',
    'F7',
    'E7',
    'D#7',
    'D7',
    'C#7',
    'C7',

    'B6',
    'A#6',
    'A6',
    'G#6',
    'G6',
    'F#6',
    'F6',
    'E6',
    'D#6',
    'D6',
    'C#6',
    'C6',

    'B5',
    'A#5',
    'A5',
    'G#5',
    'G5',
    'F#5',
    'F5',
    'E5',
    'D#5',
    'D5',
    'C#5',
    'C5',

    'B4',
    'A#4',
    'A4',
    'G#4',
    'G4',
    'F#4',
    'F4',
    'E4',
    'D#4',
    'D4',
    'C#4',
    'C4',

    'B3',
    'A#3',
    'A3',
    'G#3',
    'G3',
    'F#3',
    'F3',
    'E3',
    'D#3',
    'D3',
    'C#3',
    'C3',

    'B2',
    'A#2',
    'A2',
    'G#2',
    'G2',
    'F#2',
    'F2',
    'E2',
    'D#2',
    'D2',
    'C#2',
    'C2',

    'B1',
    'A#1',
    'A1',
    'G#1',
    'G1',
    'F#1',
    'F1',
    'E1',
    'D#1',
    'D1',
    'C#1',
    'C1',

    'B0',
    'A#0',
    'A0'
  ];
  var outputRows = [
    'C6',
    'B5',
    'A#5',
    'A5',
    'G#5',
    'G5',
    'F#5',
    'F5',
    'E5',
    'D#5',
    'D5',
    'C#5',
    'C5',
    'B4',
    'A#4',
    'A4',
    'G#4',
    'G4',
    'F#4',
    'F4',
    'E4',
    'D4',
    'C4',
    'B3',
    'A3',
    'G3',
    'D3',
    'C3'
  ];
  for(var i = 0;i < inputRows.length;i++) {
    row = table.insertRow();
    if(inputRows[i].includes('#')) {
      row.insertCell().innerHTML = '<b class=\'black\'>' + inputRows[i] + '</b>';
    }
    else {
      row.insertCell().innerHTML = '<b>' + inputRows[i] + '</b>';
    }
    for(var j = 0;j < 8;j++) {
      cell = row.insertCell();
      cell.id = 'i' + inputRows[i] + '-' + j;
    }
    if(outputRows[i]) {
      if(outputRows[i].includes('#')) {
        row.insertCell().innerHTML = '<b class=\'black\'>' + outputRows[i] + '</b>';
      }
      else {
        row.insertCell().innerHTML = '<b>' + outputRows[i] + '</b>';
      }
      for(var j = 0;j < 8;j++) {
        cell = row.insertCell();
        cell.id = 'o' + outputRows[i] + '-' + j;
        cell.onclick = onOutputClick;
      }
    }
  }
}

const showData = () => {
  table();
  if(inputData.notes) {
    for(var i = 0;i < inputData.notes.length;i++) {
      var currNote = inputData.notes[i];
      var cell = document.getElementById('i' + currNote.name + '-' + (currNote.start - inputData.start));
      cell.className = 'active';
      cell.innerText = currNote.duration;
    }
  }
  if(outputData.notes) {
    for(var i = 0;i < outputData.notes.length;i++) {
      var currNote = outputData.notes[i];
      var cell = document.getElementById('o' + currNote.name + '-' + (currNote.start - outputData.start));
      cell.className = 'active';
    }
  }
}

const loadPath = async () => {
  var path = document.getElementById('pathInput').value;
  inputData = (await axios.get(path)).data;
  outputData.start = inputData.start;
  outputData.bpm = inputData.bpm;
  outputData.notes = [];
  try {
    path = path.replace(/input/g,'output');
    outputData = (await axios.get(path)).data;
  }
  catch(err) {}
  showData();
};

const loadNextPath = async () => {
  var path = (await axios.get('/next')).data;
  document.getElementById('pathInput').value = path;
  loadPath();
};

document.getElementById('pathButton').onclick = loadPath;
document.getElementById('nextButton').onclick = loadNextPath;
document.getElementById('copyButton').onclick = () => {
  outputData = Object.assign({}, inputData);
  outputData.notes = inputData.notes.map(e => Object.assign({},e));
  outputData.notes = outputData.notes.filter(e => Object.keys(midiDictionary).includes(e.name));
  for(var i = 0;i < outputData.notes.length;i++) {
    outputData.notes[i].duration = 1;
  }
  showData();
};
document.getElementById('saveButton').onclick = async () => {
  var path = document.getElementById('pathInput').value;
  path = path.replace(/input/g,'output');
  (await axios.post('/save', {
    path: path,
    data: outputData
  }));
  loadNextPath();
}

table();
loadNextPath();