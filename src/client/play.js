const synth = new Tone.PolySynth(Tone.Synth).toDestination();

const playData = (data) => {
  var now = Tone.now();
  if(data.notes) {
    for(var i = 0;i < data.notes.length;i++) {
      var currNote = data.notes[i];
      synth.triggerAttackRelease(currNote.name, currNote.duration * 60 / data.bpm, now + (currNote.start - data.start)  * 60 / data.bpm);
    }
  }
};

const playNote = (note) => {
  synth.triggerAttackRelease(note, 0.25);
};

document.getElementById('playInputButton').onclick = () => {
  playData(inputData);
};
document.getElementById('playOutputButton').onclick = () => {
  playData(outputData);
};