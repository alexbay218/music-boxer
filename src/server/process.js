const fs = require('fs');
const { Midi } = require('@tonejs/midi');
const midiQuantize = require('./midiQuantize');
const split = require('./split');

const perPath = (path) => {
  var files = fs.readdirSync(path + '/unprocessed');
  for(var i = 0;i < files.length;i++) {
    var currMidiFile = fs.readFileSync(path + '/unprocessed/' + files[i]);
    var currMidiQuantizedData = midiQuantize(currMidiFile);
    try { fs.mkdirSync(path + '/input/' + files[i].replace(/\.mid/g,'')); } catch(err) {}
    try { fs.mkdirSync(path + '/output/' + files[i].replace(/\.mid/g,'')); } catch(err) {}
    split(currMidiQuantizedData, path + '/input/' + files[i].replace(/\.mid/g,''));
    fs.renameSync(path + '/unprocessed/' + files[i], path + '/processed/' + files[i]);
  }
}

module.exports = () => {
  perPath('data/train');
}