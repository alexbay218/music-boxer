const fs = require('fs');

module.exports = (quantized, path, splitLen = 8) => {
  var count = 0;
  for(var i = 0;i < quantized.length + splitLen;i += splitLen) {
    var data = {
      bpm: quantized.bpm,
      start: i,
      notes: []
    };
    for(var j = 0;j < quantized.notes.length;j++) {
      if(quantized.notes[j].start >= i && quantized.notes[j].start < i + splitLen) {
        data.notes.push(Object.assign({}, quantized.notes[j]));
      }
    }
    var countStr = '0000' + count;
    fs.writeFileSync(path + '/' + countStr.substr(countStr.length - 4) + '.json', JSON.stringify(data, null, 2));
    count += 1;
  }
};