const fs = require('fs');
const { Midi } = require('@tonejs/midi');
const midiQuantize = require('./midiQuantize');

var testFile = fs.readFileSync('data/test/Blue_Bird.mid');
console.log(testFile)

var testData = midiQuantize(testFile);

var newMidi = new Midi();
var track = newMidi.addTrack();
for(var i = 0;i < testData.notes.length;i++) {
  track.addNote({
    midi: testData.notes[i].midi,
    time: testData.notes[i].start*60/testData.bpm,
    duration: 60/testData.bpm,
    velocity: 1
  });
}

console.log(testData.notes);

fs.writeFileSync('data/test/output.mid', Buffer.from(newMidi.toArray()));