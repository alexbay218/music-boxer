const fs = require('fs');

const perPath = (path) => {
  var files = fs.readdirSync(path + '/processed');
  for(var i = 0;i < files.length;i++) {
    var folderName = files[i].replace(/\.mid/g,'');
    var inputFiles = fs.readdirSync(path + '/input/' + folderName);
    var outputFiles = fs.readdirSync(path + '/output/' + folderName);
    for(var j = 0;j < inputFiles.length;j++) {
      var hasOutput = false;
      for(var k = 0;k < outputFiles.length;k++) {
        if(inputFiles[j] === outputFiles[k]) {
          hasOutput = true;
        }
      }
      if(!hasOutput) {
        return path + '/input/' + folderName + '/' + inputFiles[j];
      }
    }
  }
  return '';
}

module.exports = () => {
  var path = perPath('data/train');
  if(path !== '') {
    return path;
  }
  var path = perPath('data/validate');
  if(path !== '') {
    return path;
  }
  var path = perPath('data/execute');
  if(path !== '') {
    return path;
  }
  return '';
}