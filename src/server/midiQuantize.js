const { Midi } = require('@tonejs/midi');

module.exports = (midiData) => {
  var parsedMidi = new Midi(midiData);

  var ppq = parsedMidi.header.ppq;
  var notes = [];
  var duration = 0;
  for(var i = 0;i < parsedMidi.tracks.length;i++) {
    var currTrack = parsedMidi.tracks[i];
    for(var j = 0;j < currTrack.notes.length;j++) {
      var currNote = currTrack.notes[j];
      notes.push({
        name: currNote.name,
        midi: currNote.midi,
        start: currNote.ticks/ppq,
        duration: currNote.durationTicks/ppq
      });
      if(currNote.ticks + currNote.durationTicks > duration) {
        duration = currNote.ticks + currNote.durationTicks;
      }
    }
  }
  
  var avgBpm = 0;
  var tempos = [];
  for(var i = 0;i < parsedMidi.header.tempos.length;i++) {
    var currTempo = parsedMidi.header.tempos[i];
    if(i === parsedMidi.header.tempos.length - 1) {
      tempos.push({
        bpm: currTempo.bpm,
        duration: duration - currTempo.ticks
      });
    }
    else {
      tempos.push({
        bpm: currTempo.bpm,
        duration: parsedMidi.header.tempos[i + 1].ticks - currTempo.ticks
      });
    }
  }
  
  var top = 0;
  var bottom = 0;
  for(var i = 0;i < tempos.length;i++) {
    top += tempos[i].bpm * tempos[i].duration;
    bottom += tempos[i].duration;
  }
  avgBpm = top / bottom;
  
  var count16 = 0;
  for(var i = 0;i < notes.length;i++) {
    if(notes[i].duration <= 0.125) {
      count16++;
    }
  }
  
  if(count16 > notes.length*0.1) {
    for(var i = 0;i < notes.length;i++) {
      notes[i].start = notes[i].start*2;
      notes[i].duration = notes[i].duration*2;
    }
    avgBpm = avgBpm*2;
  }
  
  duration = 0;
  for(var i = 0;i < notes.length;i++) {
    notes[i].start = Math.ceil(notes[i].start*2);
    notes[i].duration = Math.ceil(notes[i].duration*2);
    if(notes[i].start + notes[i].duration > duration) {
      duration = notes[i].start + notes[i].duration;
    }
  }
  avgBpm = Math.round(avgBpm*2);
  
  notes = notes.sort((e1, e2) => {
    return e1.start - e2.start;
  });
  return {
    bpm: avgBpm,
    notes: notes,
    length: notes.length > 0 ? notes[notes.length - 1].start + notes[notes.length - 1].duration : 0
  };
};