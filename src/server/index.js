require('./init');
require('./init');
const process = require('./process');
const path = require('path');
const express = require('express');
const next = require('./next');
const fs = require('fs');

var app = express();

app.use(express.json());

app.use(express.static(path.join(__dirname, '../client')));
app.use('/data', express.static(path.join(__dirname, '../../data')));
app.get('/process', (req, res) => {
  process();
  res.status(200).end();
});
app.get('/next', (req, res) => {
  res.status(200).send(next());
});
app.post('/save', (req, res) => {
  var data = req.body;
  fs.writeFileSync(path.join(__dirname, '../../' + data.path), JSON.stringify(data.data, null, 2));
  res.status(200).end();
});

app.listen(4000);
console.log('Listening on PORT 4000');